package com.example.moneymanager

class Item : java.io.Serializable {

    // 編號、日期、類型、說明、金額、修改、已選擇
    var id: Long = 0
    var Color: Colors
    var Date: String
    var Type: String
    var Description: String
    var Money: Long = 0
    var lastModify: Long = 0
    var isSelected: Boolean = false

    constructor() {
        Color = Colors.LIGHTGREY
        Date = ""
        Type = ""
        Description = ""
        Money = 0
    }

    constructor(id: Long, Color: Colors, Date: String, Type: String, Description: String, Money: Long, lastModify: Long) {
        this.id = id
        this.Color = Color
        this.Date = Date
        this.Type = Type
        this.Description = Description
        this.Money = Money
        this.lastModify = lastModify
    }

}