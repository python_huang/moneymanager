package com.example.moneymanager

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase

//資料功能類別
class ItemDAO(context: Context) {

    companion object {

        //表格名稱
        val TABLE_NAME = "item"

        //編號表格欄位名稱，固定不變
        val KEY_ID = "_id"

        //其他表格欄位名稱
        val COLOR_COLUMN = "color"
        val DATE_COLUMN = "date"
        val TYPE_COLUMN = "type"
        val DESCRIPTION_COLUMN = "description"
        val MONEY_COLUMN = "money"
        val LASTMODIFY_COLUMN = "lastmodify"

        // 使用上面宣告的變數建立表格的SQL敘述
        val CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLOR_COLUMN + " INTEGER NOT NULL, " +
                DATE_COLUMN + " TEXT NOT NULL, " +
                TYPE_COLUMN + " TEXT NOT NULL, " +
                DESCRIPTION_COLUMN + " TEXT, " +
                MONEY_COLUMN + " INTEGER NOT NULL, " +
                LASTMODIFY_COLUMN + " INTEGER)"

    }

    //資料庫物件
    private val db: SQLiteDatabase = MyDBHelper.getDatabase(context)

    //讀取記事資料
    val all: ArrayList<Item>
        get() {
            val result = ArrayList<Item>()
            val cursor = db.query(
                TABLE_NAME,
                null, null, null,
                null, null, null, null
            )

            while (cursor.moveToNext()) {
                result.add(getRecord(cursor))
            }

            cursor.close()
            return result
        }

    //取得資料數量
    val count: Int
        get() {
            var result = 0
            val cursor = db.rawQuery("SELECT COUNT(*) FROM" + TABLE_NAME, null)

            if (cursor.moveToNext()) {
                result = cursor.getInt(0)
            }
            return result
        }

    //關閉資料庫，一般的應用都不需要修改
    fun close() {
        db.close()
    }

    //新增參數指定的物件
    fun insert(item: Item): Item {
        //建立準備新增資料的ContentValues物件
        val cv = ContentValues()

        //加入ContentValues物件包裝的新增資料
        itemToContentValues(item, cv)

        // 新增一筆資料並取得編號
        // 第一個參數是表格名稱
        // 第二個參數是沒有指定欄位值的預設值
        // 第三個參數是包裝新增資料的ContentValues物件
        val id = db.insert(TABLE_NAME, null, cv)

        //設定編號
        item.id = id
        //回傳結果
        return item
    }

    //修改參數指定的物件
    fun update(item: Item): Boolean {
        //建立準備修改資料的ContentValues物件
        val cv = ContentValues()

        //加入ContentValues物件包裝的修改資料
        itemToContentValues(item, cv)

        // 設定修改資料的條件為編號
        // 格式為「欄位名稱＝資料」
        val where = KEY_ID + "=" + item.id

        // 執行修改資料並回傳修改的資料數量是否成功
        return db.update(TABLE_NAME, cv, where, null) > 0
    }

    private fun itemToContentValues(item: Item, cv: ContentValues) {
        //第一個參數是欄位名稱，第二個參數是欄位的資料
        cv.put(COLOR_COLUMN, item.Color.parseColor())
        cv.put(DATE_COLUMN, item.Date)
        cv.put(TYPE_COLUMN, item.Type)
        cv.put(DESCRIPTION_COLUMN, item.Description)
        cv.put(MONEY_COLUMN, item.Money)
        cv.put(LASTMODIFY_COLUMN, item.lastModify)
    }

    //刪除參數指定編號的資料
    fun delete(id: Long): Boolean {
        // 設定條件為編號，格式為「欄位名稱=資料」
        val where = KEY_ID + "=" + id
        // 刪除指定編號資料並回傳刪除是否成功
        return db.delete(TABLE_NAME, where, null) > 0
    }

    //取得指定編號的資料物件
    operator fun get(id: Long): Item? {
        //準備回傳結果用的物件
        var item: Item? = null
        //使用編號為查詢條件
        val where = KEY_ID + "=" + id
        //執行查詢
        val result = db.query(
            TABLE_NAME, null, where, null, null,
            null, null, null
        )

        //如有查詢結果
        if (result.moveToFirst()) {
            //讀取包裝一筆資料的物件
            item = getRecord(result)
        }

        //關閉Cursor物件
        result.close()
        //回傳結果
        return item
    }

    //把Cursor目前的資料包裝為物件
    fun getRecord(cursor: Cursor): Item {
        //準備回傳結果用的物件
        val result = Item()

        result.id = cursor.getLong(0)
        result.Color = ItemActivity.getColors(cursor.getInt(1))
        result.Date = cursor.getString(2)
        result.Type = cursor.getString(3)
        result.Description = cursor.getString(4)
        result.Money = cursor.getLong(5)
        result.lastModify = cursor.getLong(6)

        //回傳結果
        return result
    }

    fun queryAll(): ArrayList<Item> {
        val AllItem = ArrayList<Item>()
        //查詢全部數據  select * from person
        val rawQueryAllSql = "SELECT * FROM item"
        val cursor = db.rawQuery(rawQueryAllSql, null)
        if (cursor != null) {
            while (cursor.moveToNext()) {
                val id = cursor.getLong(0)
                val Color = ItemActivity.getColors(cursor.getInt(1))
                val Date = cursor.getString(2)
                val Type = cursor.getString(3)
                val Description = cursor.getString(4)
                val Money = cursor.getLong(5)
                val Lastmodify = cursor.getLong(6)
                val DataMonth = Item(id, Color, Date, Type, Description, Money, Lastmodify)
                AllItem.add(DataMonth)
            }
        }
        cursor.close()
        return AllItem
    }

    fun queryMonth(month:String): ArrayList<Item> {
        val MonthItem = ArrayList<Item>()
        //查詢數據
        val rawQueryMonthSql = "SELECT * FROM item WHERE strftime('%m', date) = ?"
        val cursor = db.rawQuery(rawQueryMonthSql, arrayOf("$month"))
        if (cursor != null) {
            while (cursor.moveToNext()) {
                val id = cursor.getLong(0)
                val Color = ItemActivity.getColors(cursor.getInt(1))
                val Date = cursor.getString(2)
                val Type = cursor.getString(3)
                val Description = cursor.getString(4)
                val Money = cursor.getLong(5)
                val Lastmodify = cursor.getLong(6)
                val DataMonth = Item(id, Color, Date, Type, Description, Money, Lastmodify)
                MonthItem.add(DataMonth)
            }
        }
        cursor.close()
        return MonthItem
    }

}