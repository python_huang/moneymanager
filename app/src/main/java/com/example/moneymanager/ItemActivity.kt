package com.example.moneymanager

import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import android.widget.TableLayout
import kotlinx.android.synthetic.main.single_item.*
import java.text.SimpleDateFormat
import java.util.*

class ItemActivity : AppCompatActivity() {

    private val DateText: EditText by bind(R.id.item_container_date)
    private val TypeText: EditText by bind(R.id.item_container_type)
    private val DescriptionText: EditText by bind(R.id.item_container_description)
    private val MoneyText: EditText by bind(R.id.item_container_money)

    // 啟動功能用的請求代碼
    enum class ItemAction { COLOR }

    //記事物件
    private var item: Item = Item()

    private val calender = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)

        //修改記事
        Edit()
        //日期選擇框
        item_container_date.setOnClickListener(ItemcContainerDateListener)
        //類型選擇框
        item_container_type.setOnClickListener(item_container_typeListener)

    }

    //如果修改記事
    fun Edit() {
        // 讀取Action名稱
        val action = intent.action

        // 如果是修改記事
        if (action == "com.example.moneymanager.EDIT_ITEM") {
            // 接收記事物件與設定日期、類型、說明、金額
            item = intent.extras.getSerializable("com.example.moneymanager.Item") as Item
            DateText.setText(item.Date)
            TypeText.setText(item.Type)
            DescriptionText.setText(item.Description)
            MoneyText.setText(item.Money.toString())

            // 根據記事物件的顏色設定畫面的背景顏色
            findViewById<TableLayout>(R.id.item_container).setBackgroundColor(item.Color.parseColor())
        }
    }

    // 點擊確定與取消按鈕都會呼叫這個函式
    fun onSubmit(view: View) {
        // 確定按鈕
        if (view.id == R.id.item_container_ok) {
            // 讀取使用者輸入的日期、類型、說明與金額
            val DateText = DateText.text.toString()
            val TypeText = TypeText.text.toString()
            val DescriptionText = DescriptionText.text.toString()
            val MoneyText = MoneyText.text.toString()

            // 設定日期、類型、說明與金額
            item.Date = DateText
            item.Type = TypeText
            item.Description = DescriptionText
            item.Money = MoneyText.toLong()

            if (intent.action == "com.example.moneymanager.EDIT_ITEM") {
            } else {
                // 建立SharedPreferences物件
                val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
                // 讀取設定的預設顏色
                val color = sharedPreferences.getInt("DEFAULT_COLOR", -1)
                item.Color = getColors(color)
            }

            // 設定回傳的記事物件
            intent.putExtra("com.example.moneymanager.Item", item)
            setResult(Activity.RESULT_OK, intent)
        } else {
            // 設定回應結果為取消
            setResult(Activity.RESULT_CANCELED, intent)
        }

        // 結束
        finish()
    }

    // 更改參數data的型態為Intent?
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {

            val actionRequest = ItemAction.values()[requestCode]

            when (actionRequest) {
                // 設定顏色
                ItemAction.COLOR -> {
                    if (data != null) {
                        val colorId = data.getIntExtra(
                            "colorId", Colors.LIGHTGREY.parseColor()
                        )
                        item.Color = getColors(colorId)
                        // 根據選擇的顏色設定畫面的背景顏色
                        findViewById<TableLayout>(R.id.item_container)
                            .setBackgroundColor(item.Color.parseColor())
                    }
                }
            }

        }
    }

    // 改為可以使用類別名稱呼叫這個函式
    companion object {
        // 轉換顏色值為Colors型態
        public fun getColors(color: Int): Colors {
            var result = Colors.LIGHTGREY

            if (color == Colors.BLUE.parseColor()) {
                result = Colors.BLUE
            } else if (color == Colors.PURPLE.parseColor()) {
                result = Colors.PURPLE
            } else if (color == Colors.GREEN.parseColor()) {
                result = Colors.GREEN
            } else if (color == Colors.ORANGE.parseColor()) {
                result = Colors.ORANGE
            } else if (color == Colors.RED.parseColor()) {
                result = Colors.RED
            }

            return result
        }
    }

    fun clickFunction(view: View) {

        when (view.id) {
            //選擇設定顏色功能
            R.id.select_color_button -> {
                // 啟動設定顏色的Activity元件
                startActivityForResult(
                    Intent(this, ColorActivity::class.java),
                    ItemAction.COLOR.ordinal
                )
            }
        }

    }

    // 使用者選擇返回鍵
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 設定回應結果為取消
            setResult(Activity.RESULT_CANCELED, intent)
        }
        return super.onKeyDown(keyCode, event)

    }

    private val ItemcContainerDateListener = View.OnClickListener {
        when (it) {
            item_container_date -> {
                datePicker()
            }
        }

    }

    private fun datePicker() {
        DatePickerDialog(
            this,
            dateListener,
            calender.get(Calendar.YEAR),
            calender.get(Calendar.MONTH),
            calender.get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    private val dateListener = DatePickerDialog.OnDateSetListener { _, year, month, day ->
        calender.set(year, month, day)
        format("YYYY-MM-dd", item_container_date)
    }

    private fun format(format: String, view: View) {
        val time = SimpleDateFormat(format, Locale.TAIWAN)
        (view as EditText).setText(time.format(calender.time))
    }

    private val item_container_typeListener = View.OnClickListener {

        val listItems = resources.getStringArray(R.array.Type_Array)
        val mBuilder = AlertDialog.Builder(this@ItemActivity)
        mBuilder.setTitle("類型")
        mBuilder.setSingleChoiceItems(listItems, -1) { dialogInterface, i ->
            item_container_type.text = listItems[i]
            dialogInterface.dismiss()
        }
        mBuilder.setNegativeButton("Cancel") { dialog, which ->
            dialog.cancel()
        }

        val mDialog = mBuilder.create()
        mDialog.show()

    }

}