package com.example.moneymanager.Alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val intentGetTime = Intent(context, AlarmAlert::class.java)
        val bundle = Bundle()
        bundle.putString("STR_CALLER", "")
        intentGetTime.putExtras(bundle)
        intentGetTime.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intentGetTime)

        val time = intent.getStringExtra("time")
        val operator = DataBaseOperator(context)
        if (time != null) {
            if (operator.deleteAlarm(time) > 0) {
                val pre = LampSharePreference.getInstance(context)
                var nums = pre.getInt(LampSharePreference.ALARM_NUMBERS, 0)
                nums--
                pre.setInt(LampSharePreference.ALARM_NUMBERS, nums)
            }
        }

    }

}