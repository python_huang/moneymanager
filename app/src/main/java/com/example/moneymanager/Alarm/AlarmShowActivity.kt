package com.example.moneymanager.Alarm

import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.example.moneymanager.R


class AlarmShowActivity : AppCompatActivity(), View.OnClickListener,
    AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private var listView: ListView? = null  //alarm show list
    private var iButton: ImageButton? = null  //add clock button
    private var cursorAdapter: SimpleCursorAdapter? = null
    private var dbOpeater: DataBaseOperator? = null
    private var mCursor: Cursor? = null  //數據庫指針

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alarm_show_view)
        listView = findViewById<View>(R.id.listView) as ListView
        listView!!.onItemClickListener = this
        listView!!.onItemLongClickListener = this
        iButton = findViewById<View>(R.id.add_button) as ImageButton
        iButton!!.setOnClickListener(this)
        dbOpeater = DataBaseOperator(this)  //數據庫對象
        setupView()
    }

    override fun onResume() {
        super.onResume()
        mCursor = dbOpeater!!.query(MyDataBaseHelper.ALARM_TB_NAME)  //獲得alarm的table
        val colums = arrayOf(
            MyDataBaseHelper.COL_TIME,
            MyDataBaseHelper.COL_ALARM_STATUS,
            MyDataBaseHelper.COL_ALARM_REPEAT_TIMES
        )
        val layoutsId = intArrayOf(R.id.alarm_time, R.id.alarm_status)
        cursorAdapter = SimpleCursorAdapter(
            this, R.layout.alarm_item, mCursor, colums, layoutsId, CursorAdapter.FLAG_AUTO_REQUERY
        )
        listView!!.adapter = cursorAdapter
    }

    override fun onStop() {
        mCursor!!.close()
        super.onStop()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.add_button -> {
                val intent = Intent(this, AlarmEditActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        val intent = Intent(this, AlarmEditActivity::class.java)
        val modify_time = view.findViewById<View>(R.id.alarm_time) as TextView
        intent.putExtra("time", modify_time.text)
        intent.putExtra("position", position + 1)
        startActivity(intent)

    }

    override fun onItemLongClick(parent: AdapterView<*>, view: View, position: Int, id: Long): Boolean {
        val intent = Intent(this, DeleteAlarmActivity::class.java)
        startActivity(intent)
        return false
    }

    private fun setupView() {
        title = "鬧鐘"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}