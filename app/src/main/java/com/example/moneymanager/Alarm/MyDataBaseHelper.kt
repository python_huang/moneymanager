package com.example.moneymanager.Alarm

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class MyDataBaseHelper(context: Context, name: String, factory: SQLiteDatabase.CursorFactory?, version: Int)
    : SQLiteOpenHelper(context, name, factory, version) {

    private fun createTable(db: SQLiteDatabase) {
        db.execSQL(CREATE_ACCOUNT_TABLE)
        db.execSQL(CREATE_ALARM_TABLE)
        db.execSQL(CREATE_LAMP_TABLE)
    }

    override fun onCreate(db: SQLiteDatabase) {
        createTable(db)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }

    companion object {

        var helper: MyDataBaseHelper? = null

        private val DB_VERSION = 2
        val COL_ID = "_id"
        val DB_NAME = "wrist.db"
        val ACCOUNT_TB_NAME = "account_tb"
        val COl_ACCOUNTS = "account"
        val COL_PASSWORD = "password"

        val ALARM_TB_NAME = "alarm_tb"
        val COL_TIME = "alarm_time"
        val COL_ALARM_STATUS = "alarm_status"
        val COL_ALARM_REPEAT_TIMES = "alarm_times"

        val LAMP_TB_NAME = "lamp_tb"
        val COL_LAMP_DATA_TIME = "data_time"
        val COL_LAMP_DATA_TEMPERATUE = "data_temperature"
        val COL_LAMP_DATA_HUMIDITY = "data_humidity"
        val COL_LAMP_DATA_NOISE = "data_noise"

        private val CREATE_ACCOUNT_TABLE = ("CREATE TABLE " + ACCOUNT_TB_NAME
                + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COl_ACCOUNTS + " TEXT NOT NULL,"
                + COL_PASSWORD + " TEXT NOT NULL);")

        private val CREATE_ALARM_TABLE = ("CREATE TABLE " + ALARM_TB_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COL_ALARM_STATUS + " TEXT NOT NULL,"
                + COL_ALARM_REPEAT_TIMES + " Text NOT NULL,"
                + COL_TIME + " TEXT NOT NULL);")

        private val CREATE_LAMP_TABLE = ("CREATE TABLE " + LAMP_TB_NAME
                + "(" + COL_ID + " INTEGER PRIMARY KEY,"
                + COL_LAMP_DATA_TIME + " TEXT NOT NULL,"
                + COL_LAMP_DATA_TEMPERATUE + " REAL,"
                + COL_LAMP_DATA_NOISE + " INTEGER,"
                + COL_LAMP_DATA_HUMIDITY + " REAL);")

        @Synchronized
        fun getInstance(context: Context): MyDataBaseHelper {
            if (helper == null) {
                helper = MyDataBaseHelper(context, DB_NAME, null, DB_VERSION)
            }
            return helper as MyDataBaseHelper
        }

    }

}