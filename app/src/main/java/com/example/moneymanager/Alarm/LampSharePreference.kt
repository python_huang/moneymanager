package com.example.moneymanager.Alarm

import android.content.Context
import android.content.SharedPreferences

class LampSharePreference private constructor(context: Context) {
    private val spre: SharedPreferences
    private val editor: SharedPreferences.Editor

    init {
        spre = context.getSharedPreferences(LAMP_PRE, Context.MODE_PRIVATE)
        editor = spre.edit()
    }

    fun setInt(key: String, defValue: Int) {
        editor.putInt(key, defValue)
        editor.commit()
    }

    fun getInt(key: String, defValue: Int): Int {
        return spre.getInt(key, defValue)
    }

    companion object {

        var lampSharePreference: LampSharePreference? = null

        private val LAMP_PRE = "lamp_pre"
        val ALARM_NUMBERS = "alarm_numbers"

        fun getInstance(context: Context): LampSharePreference {
            if (lampSharePreference == null) {
                lampSharePreference = LampSharePreference(context)
            }
            return lampSharePreference as LampSharePreference
        }
    }

}