package com.example.moneymanager.Alarm

import android.content.Context
import android.database.Cursor
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.CursorAdapter
import android.widget.ImageButton
import android.widget.ListView
import android.widget.SimpleCursorAdapter
import com.example.moneymanager.R

class DeleteAlarmActivity : AppCompatActivity() {

    internal lateinit var listView: ListView
    internal lateinit var adapter: MyListViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alarm_delete_view)
        listView = findViewById<View>(R.id.delete_alarm_list) as ListView
        initData()
        setupView()
    }

    private fun initData() {
        val helper = MyDataBaseHelper.getInstance(this)
        val dWriter = helper.writableDatabase
        val cursor =
            dWriter.query(MyDataBaseHelper.ALARM_TB_NAME, null, null, null, null, null, null)
        val alarmColums =
            arrayOf(MyDataBaseHelper.COL_TIME, MyDataBaseHelper.COL_ALARM_REPEAT_TIMES)
        val layoutId = intArrayOf(R.id.alarm_delete_time)
        adapter = MyListViewAdapter(
            this, R.layout.alarm_delete_item, cursor, alarmColums, layoutId, CursorAdapter.FLAG_AUTO_REQUERY
        )
        listView.adapter = adapter

    }

    inner class MyListViewAdapter(
        context: Context, layout: Int, c: Cursor, from: Array<String>, to: IntArray, flags: Int)
        : SimpleCursorAdapter(context, layout, c, from, to, flags) {
        override fun bindView(view: View, context: Context, cursor: Cursor) {
            super.bindView(view, context, cursor)
            val id = cursor.getInt(0)
            val deleteButton = view.findViewById<View>(R.id.alarm_delete_button) as ImageButton
            deleteButton.setOnClickListener {
                val operator = DataBaseOperator(context)
                val pre = LampSharePreference.getInstance(context)
                var nums = pre.getInt(LampSharePreference.ALARM_NUMBERS, 0)
                nums--
                pre.setInt(LampSharePreference.ALARM_NUMBERS, 0)
                operator.delete(id)
                cursor.requery()
                adapter.notifyDataSetChanged()
            }
        }

    }

    private fun setupView() {
        title = "刪除鬧鐘"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}