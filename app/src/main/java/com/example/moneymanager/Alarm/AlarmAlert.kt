package com.example.moneymanager.Alarm

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import com.example.moneymanager.R

class AlarmAlert : Activity() {

    lateinit var manager: NotificationManager
    lateinit var builder : Notification.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AlertDialog.Builder(this@AlarmAlert)
            .setTitle("記帳")
            .setMessage("記帳時間到了！")
            .setPositiveButton("關掉") { _, _ ->
                this@AlarmAlert.finish()
                startActivity(Intent(this, AlarmShowActivity::class.java))
            }.show()
        /*createNotificationChannel()
        noti()
        manager.notify(0, builder.build())*/
    }

    fun noti() {
        manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("MoneyManager", "MoneyManager", NotificationManager.IMPORTANCE_HIGH)
            manager.createNotificationChannel(channel)
            builder = Notification.Builder(this, "MoneyManager")
        } else {
            builder = Notification.Builder(this)
        }

        builder.setSmallIcon(R.drawable.ic_pets)
            .setContentTitle("記帳")
            .setContentText("記帳時間到了")
            .setAutoCancel(true)
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("channel id test", name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

}