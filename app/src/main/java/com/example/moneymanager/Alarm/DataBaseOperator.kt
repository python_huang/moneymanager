package com.example.moneymanager.Alarm

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase

class DataBaseOperator(context: Context) {

    internal var helper: MyDataBaseHelper
    internal var dbWriter: SQLiteDatabase

    init {
        helper = MyDataBaseHelper.getInstance(context)
        dbWriter = helper.writableDatabase
    }

    /*
    向數據庫裡插入數據
     */
    fun intsert(tbName: String, values: ContentValues) {
        dbWriter.insert(tbName, null, values)
    }

    /*
    get table
     */
    fun query(tb: String): Cursor {
        return dbWriter.query(tb, null, null, null, null, null, null)
    }

    fun update(table: String, values: ContentValues, whereClause: String, whereArgs: Array<String?>): Int {
        return dbWriter.update(table, values, whereClause, whereArgs)
    }

    fun delete(id: Int) {
        val args = arrayOf(id.toString())
        dbWriter.delete(MyDataBaseHelper.ALARM_TB_NAME, "_id=?", args)
    }

    fun deleteAlarm(time: String): Int {
        val args = arrayOf(time)
        return dbWriter.delete(MyDataBaseHelper.ALARM_TB_NAME, "alarm_time=?", args)
    }

}