package com.example.moneymanager.Alarm

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object TimeTool {

    fun turnStringToDate(sTime: String): Date? {
        val format = SimpleDateFormat("HH:mm:ss")
        var now: Date? = null
        try {
            now = format.parse(sTime)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return now
    }

    fun turnDateToString(date: Date): String {
        val format = SimpleDateFormat("HH:mm:ss")
        val time: String
        time = format.format(date)
        return time

    }

    fun turnDateToStringonlyTime(date: Date): String {
        val format = SimpleDateFormat("HH:mm")
        val time: String
        time = format.format(date)
        return time
    }

}