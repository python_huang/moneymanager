package com.example.moneymanager.Alarm

import java.util.*

class Alarm @JvmOverloads constructor(time: Calendar = Calendar.getInstance()) {

    var whichAlarm = 1
    var command = "time"
    var time: String? = null
    var alarmStatus = ""
    var repeatTimes = "僅一次"
    var alarmChangeWay = "new"//modify和new兩種值

    init {
        this.time = TimeTool.turnDateToString(time.time)
    }

    fun getAlarmTime(): String? {
        return time
    }
}