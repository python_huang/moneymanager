package com.example.moneymanager.Alarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.example.moneymanager.R
import java.util.*

class AlarmEditActivity : AppCompatActivity(), View.OnClickListener,
    AdapterView.OnItemClickListener {

    private var listView: ListView? = null
    private var datalist: MutableList<Map<String, String>>? = null
    private var map: MutableMap<String, String>? = null
    private var simpleAdapter: SimpleAdapter? = null
    private var alarm: Alarm? = null
    private lateinit var dbOperator: DataBaseOperator

    private lateinit var time_calender: Calendar
    private var modify_time_string: String? = null

    private lateinit var alarmPre: LampSharePreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alarm_edit)

        alarmPre = LampSharePreference.getInstance(this)
        dbOperator = DataBaseOperator(this)
        modify_time_string = intent.getStringExtra("time")

        alarm = Alarm()

        if (modify_time_string != null) {
            alarm!!.alarmChangeWay = "modify"
            alarm!!.time = modify_time_string
            alarm!!.whichAlarm = intent.getIntExtra("position", 0)
        } else {
            alarm!!.alarmChangeWay = "new"
            alarm!!.whichAlarm = alarmPre.getInt(LampSharePreference.ALARM_NUMBERS, 0) + 1
        }
        datalist = ArrayList()

        val saveAlarm = findViewById<View>(R.id.save_alarm) as ImageButton
        saveAlarm.setOnClickListener(this)

        val date = TimeTool.turnStringToDate(alarm!!.getAlarmTime()!! + ":00")

        time_calender = Calendar.getInstance()
        time_calender.set(Calendar.HOUR_OF_DAY, date!!.hours)  //鬧鐘時間，新建時為當前時間
        time_calender.set(Calendar.MINUTE, date.minutes)

        setupView()

    }

    override fun onResume() {
        super.onResume()
        val timePicker = findViewById<View>(R.id.timePicker) as TimePicker
        timePicker.setIs24HourView(true)
        timePicker.currentHour = time_calender.time.hours
        timePicker.currentMinute = time_calender.time.minutes
        timePicker.setOnTimeChangedListener { _, hourOfDay, minuite ->
            time_calender.timeInMillis = System.currentTimeMillis()
            time_calender.timeZone = TimeZone.getTimeZone("GMT+8")
            time_calender.set(Calendar.HOUR_OF_DAY, hourOfDay)
            time_calender.set(Calendar.MINUTE, minuite)
            time_calender.set(Calendar.SECOND, 0)
            time_calender.set(Calendar.MILLISECOND, 0)
            if (time_calender.before(Calendar.getInstance())) {
                time_calender.add(Calendar.DAY_OF_MONTH, 1)
            }
        }

        listView = findViewById<View>(R.id.alarm_edit_list_view) as ListView
        setDataList()
        listView!!.adapter = simpleAdapter
        listView!!.onItemClickListener = this
    }

    fun setDataList() {
        datalist!!.clear()
        map = HashMap()
        map!!["name"] = "重複次數"
        map!!["value"] = alarm!!.repeatTimes
        datalist!!.add(map as HashMap<String, String>)
        map = HashMap()
        map!!["name"] = "目標"
        map!!["value"] = alarm!!.alarmStatus
        datalist!!.add(map as HashMap<String, String>)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.save_alarm -> {
                saveAlarm()
                finish()
            }
        }
    }

    private fun saveAlarm() {

        val time_string = TimeTool.turnDateToStringonlyTime(time_calender.time)
        if (time_calender.before(Calendar.getInstance())) {  //如果時間早於現在就是天數+1
            time_calender.set(Calendar.DAY_OF_MONTH, time_calender.get(Calendar.DAY_OF_MONTH) + 1)
        }
        val values = ContentValues()
        values.put(MyDataBaseHelper.COL_TIME, time_string)
        values.put(MyDataBaseHelper.COL_ALARM_STATUS, alarm!!.alarmStatus)
        values.put(MyDataBaseHelper.COL_ALARM_REPEAT_TIMES, alarm!!.repeatTimes)

        val intent = Intent(this, AlarmReceiver::class.java)  //啟動broadcast的intent
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        if (alarm!!.alarmChangeWay == "new") {

            dbOperator.intsert(MyDataBaseHelper.ALARM_TB_NAME, values)
            intent.putExtra("time", time_string)
            //更新鬧鐘數量
            alarmPre.setInt(LampSharePreference.ALARM_NUMBERS, alarm!!.whichAlarm)

        } else {

            dbOperator.update(
                MyDataBaseHelper.ALARM_TB_NAME, values, "alarm_time = ?", arrayOf(modify_time_string)
            )
            intent.putExtra("time", time_string)

        }

        val pendingIntent =
            PendingIntent.getBroadcast(this, alarm!!.whichAlarm,
                intent, PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time_calender.timeInMillis, pendingIntent)
//            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time_calender.timeInMillis, 1000 * 60, pendingIntent)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, time_calender.timeInMillis, pendingIntent)
//            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time_calender.timeInMillis, 1000 * 60, pendingIntent)
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, time_calender.timeInMillis, pendingIntent)
//            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time_calender.timeInMillis, 1000 * 60, pendingIntent)
        }

        alarm!!.time = time_string
        alarm!!.command = "alarm"

    }

    /*fun Cancel() {
        val intent = Intent(this, AlarmReceiver::class.java)
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntent =
            PendingIntent.getBroadcast(this, alarm!!.whichAlarm,
                intent, PendingIntent.FLAG_UPDATE_CURRENT)
        alarmManager.cancel(pendingIntent)
    }*/

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {

    }

    private fun setupView() {
        title = "設定鬧鐘"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}