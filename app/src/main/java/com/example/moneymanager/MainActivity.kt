package com.example.moneymanager

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.example.moneymanager.Alarm.AlarmShowActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var drawer: DrawerLayout
    lateinit var toolbar: Toolbar
    lateinit var navigationView: NavigationView

    //加入下列需要的元件
    private val item_list: RecyclerView by bind(R.id.item_list)
    private lateinit var itemAdapter: RecyclerView.Adapter<ItemAdapterRV.ViewHolder>
    private val rvLayoutManager: RecyclerView.LayoutManager
            by lazy { LinearLayoutManager(this) }

    // 儲存所有記事本的List物件
    private val items: ArrayList<Item> = ArrayList()

    // 選單項目物件
    private lateinit var revert_item: MenuItem
    private lateinit var delete_item: MenuItem

    // 已選擇項目數量
    private var selectedCount = 0

    //宣告資料庫功能類別欄位變數
    private val itemDAO: ItemDAO by lazy { ItemDAO(applicationContext) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //取得所有記事資料
        items.addAll(itemDAO.all)

        //執行RecyclerView元件的設定
        item_list.setHasFixedSize(true)
        item_list.layoutManager = rvLayoutManager

        //在這裡執行註冊監聽事件的工作
        processControllers()
        //側邊欄的集成
        toggleView()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // 如果被啟動的Activity元件傳回確定的結果
        if (resultCode == Activity.RESULT_OK) {
            // 讀取記事物件
            val item = data!!.extras.getSerializable("com.example.moneymanager.Item") as Item

            // 如果是新增記事
            if (requestCode === 0) {
                //新增記事資料到資料庫
                val itemNew: Item = itemDAO.insert(item)
                // 設定記事物件的編號
                item.id = itemNew.id

                // 加入新增的記事物件
                items.add(item)

                // 通知資料改變
                itemAdapter.notifyDataSetChanged()

            }

            // 如果是修改記事
            else if (requestCode == 1) {
                // 讀取記事編號
                val position = data.getIntExtra("position", -1)

                if (position != -1) {
                    // 修改資料庫中的記事資料
                    itemDAO.update(item)

                    items.set(position, item)
                    itemAdapter.notifyDataSetChanged()
                }
            }

        }
    }

    private fun processControllers() {
        //實作ItemAdapterRV類別，加入註冊監聽事件的工作
        itemAdapter = object : ItemAdapterRV(items) {
            override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)

                //建立與註冊項目點擊監聽物件
                holder.rootView.setOnClickListener {
                    //讀取選擇的記事物件
                    val item = items[position]

                    //如果已經有勾選的項目
                    if (selectedCount > 0) {
                        //處理是否顯示已選擇項目
                        processMenu(item)
                        //重新設定記事項目
                        items[position] = item
                    } else {
                        val intent = Intent("com.example.moneymanager.EDIT_ITEM")

                        //設定記事編號與記事物件
                        intent.putExtra("position", position)
                        intent.putExtra("com.example.moneymanager.Item", item)

                        //依照版本啟動Activity元件
                        startActivityForVersion(intent, 1)
                    }
                }

                //建立與註冊項目長按監聽物件
                holder.rootView.setOnLongClickListener {
                    // 讀取選擇的記事物件
                    val item = items[position]
                    // 處理是否顯示已選擇項目
                    processMenu(item)
                    // 重新設定記事項目
                    items[position] = item
                    true
                }
            }
        }

        //設定RecyclerView使用的資料來源
        item_list.adapter = itemAdapter
    }

    // 處理是否顯示已選擇項目
    private fun processMenu(item: Item?) {
        //如果需要設定記事
        if (item != null) {
            //設定已勾選狀態
            item.isSelected = !item.isSelected
            //計算已勾選數量
            if (item.isSelected) {
                selectedCount++
            } else {
                selectedCount--
            }
        }

        //根據選擇情況，設定是否顯示選單項目
        revert_item.setVisible(selectedCount > 0)
        delete_item.setVisible(selectedCount > 0)

        //通知項目勾選狀態改變
        itemAdapter.notifyDataSetChanged()
    }

    // 載入選單資源
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        //取得選單項目物件
        revert_item = menu.findItem(R.id.revert_item)
        delete_item = menu.findItem(R.id.delete_item)

        //設定選單項目
        processMenu(null)

        return true
    }

    //點擊新增按鈕
    fun clickAdd(view: View) {
        val intent = Intent("com.example.moneymanager.ADD_ITEM")
        startActivityForVersion(intent, 0)
    }

    // 使用者選擇所有的選單項目都會呼叫這個函式
    fun clickMenuItem(item: MenuItem) {
        // 判斷該執行什麼工作，目前還沒有加入需要執行的工作
        when (item.itemId) {
            //取消所有已勾選的項目
            R.id.revert_item -> {
                //改為使用items物件
                for (i in 0 until items.size) {
                    val ri = items[i]

                    if (ri.isSelected) {
                        ri.isSelected = false
                    }
                }

                selectedCount = 0
                processMenu(null)
            }
            //刪除
            R.id.delete_item -> {
                //沒有選擇
                if (selectedCount == 0) {
                    return
                }

                //建立與顯示詢問是否刪除的對話框
                val d = AlertDialog.Builder(this)
                val message = getString(R.string.delete_item)
                d.setTitle(R.string.delete).setMessage(String.format(message, selectedCount))
                d.setPositiveButton(android.R.string.yes) { _, _ ->
                    //刪除所有已勾選的項目
                    var index = items.size - 1

                    while (index > -1) {
                        val item = items[index]

                        if (item.isSelected) {
                            items.remove(item)
                            //刪除資料庫中的記事資料
                            itemDAO.delete(item.id)
                        }

                        index--
                    }

                    selectedCount = 0
                    processMenu(null)
                }
                d.setNegativeButton(android.R.string.no, null)
                d.show()
            }
        }
    }

    // 設定
    fun clickPreferences(item: MenuItem) {
        // 啟動設定元件
        startActivity(Intent(this, PrefActivity::class.java))
    }

    private fun startActivityForVersion(intent: Intent, requestCode: Int) {
        //如果裝置的版本是LOLLIPOP
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //加入畫面轉換設定
            startActivityForResult(
                intent, requestCode,
                ActivityOptions.makeSceneTransitionAnimation(this@MainActivity)
                    .toBundle()
            )
        } else {
            startActivityForResult(intent, requestCode)
        }
    }

    //左側邊欄集合圖示
    private fun toggleView() {
        drawer = findViewById(R.id.drawerLayout)
        toolbar = findViewById(R.id.toolbar)
        navigationView = findViewById(R.id.navigation_view)

        val toggle = ActionBarDrawerToggle(this, drawer, toolbar,
            R.string.drawer_open, R.string.drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        setSupportActionBar(toolbar)

    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> drawer.openDrawer(GravityCompat.START)
        }
        return super.onOptionsItemSelected(item)
    }

    //左側邊欄功能
    fun clickSideMenu(item: MenuItem) {
        when (item.itemId) {

            R.id.action_home -> {
                startActivity(Intent(this, MainActivity::class.java))
            }
            R.id.action_time -> {
                startActivity(Intent(this, AlarmShowActivity::class.java))
            }

        }
    }

    //顯示分類資料
    override fun onResume() {
        super.onResume()
        FilterData()
    }

    //分類資料功能
    private fun FilterData() {

        val share = PreferenceManager.getDefaultSharedPreferences(this)
        val Month = share.getString("DEFAULT_FILTER", "00")

        when (Month) {
            "00" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryAll())
                itemAdapter.notifyDataSetChanged()
            }
            "01" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("01"))
                itemAdapter.notifyDataSetChanged()
            }
            "02" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("02"))
                itemAdapter.notifyDataSetChanged()
            }
            "03" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("03"))
                itemAdapter.notifyDataSetChanged()
            }
            "04" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("04"))
                itemAdapter.notifyDataSetChanged()
            }
            "05" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("05"))
                itemAdapter.notifyDataSetChanged()
            }
            "06" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("06"))
                itemAdapter.notifyDataSetChanged()
            }
            "07" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("07"))
                itemAdapter.notifyDataSetChanged()
            }
            "08" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("08"))
                itemAdapter.notifyDataSetChanged()
            }
            "09" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("09"))
                itemAdapter.notifyDataSetChanged()
            }
            "10" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("10"))
                itemAdapter.notifyDataSetChanged()
            }
            "11" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("11"))
                itemAdapter.notifyDataSetChanged()
            }
            "12" -> {
                Log.d("VALUE", "value=${Month}")
                items.clear()
                items.addAll(itemDAO.queryMonth("12"))
                itemAdapter.notifyDataSetChanged()
            }
        }
    }

}