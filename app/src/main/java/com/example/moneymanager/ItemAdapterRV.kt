package com.example.moneymanager

import android.graphics.drawable.GradientDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView

open class ItemAdapterRV(private val items: MutableList<Item>)
    :RecyclerView.Adapter<ItemAdapterRV.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.single_item, parent, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        //設定記事顏色
        val background = holder.typeColor.background as GradientDrawable
        background.setColor(item.Color.parseColor())

        //設定標題與日期時間
        holder.dateView.text = item.Date
        holder.typeView.text = item.Type
        holder.descriptionView.text = item.Description
        holder.moneyView.text = item.Money.toString()

        //設定是否已選擇
        holder.selectedItem.visibility =
            if (item.isSelected) View.VISIBLE else View.INVISIBLE
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun add (item: Item) {
        items.add(item)
        notifyItemInserted(items.size)
    }

    // 一定要使用ViewHolder包裝畫面元件
    inner class ViewHolder(var rootView: View)
        :RecyclerView.ViewHolder(rootView) {

        var typeColor: RelativeLayout = itemView.findViewById(R.id.selected_color)
        var selectedItem: ImageView = itemView.findViewById(R.id.selected_item)
        var dateView: TextView = itemView.findViewById(R.id.item_container_date)
        var typeView : TextView = itemView.findViewById(R.id.item_container_type)
        var descriptionView : TextView = itemView.findViewById(R.id.item_container_description)
        var moneyView : TextView = itemView.findViewById(R.id.item_container_money)
    }

}